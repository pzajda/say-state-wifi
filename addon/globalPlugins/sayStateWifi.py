# -*- coding: utf-8 -*-
# Wifi (Intel Wireless with Dell) announcement Global Plugin for NVDA
# Authors: Filaos, Patrick ZAJDA
# Shortcut: NVDA+Shift+W

import addonHandler,globalPluginHandler,IAccessibleHandler
import scriptHandler
from ui import message
import ctypes
import NVDAObjects
from api import getFocusObject
import winUser

addonHandler.initTranslation()

class GlobalPlugin(globalPluginHandler.GlobalPlugin):

	def script_announceWireless(self, gesture):
		l=("shell_TrayWnd","TrayNotifyWnd","SysPager","ToolbarWindow32")
		h,FindWindowExA =0,winUser.user32.FindWindowExA
		for element in l:
			h=FindWindowExA(h,0,element,0)

		o=NVDAObjects.IAccessible.getNVDAObjectFromEvent(h,-4,1)
		name=o.name
		while (name.lower()).find (u'intel\xae proset/wireless wifi')==-1:
			o=o.next
			if not o :
				message(_("Intel (R) ProSet/Wireless WiFi not found, or Wi-Fi is not connected"))
				return
			else:
				name=o.name

		message (name)
	# Documentation
	script_announceWireless.__doc__ = _("Announce Intel(R) ProSet/Wireless WiFi's status")

	__gestures={
		"kb:NVDA+shift+W": "announceWireless",
	}

